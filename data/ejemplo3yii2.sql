DROP DATABASE IF EXISTS ejemployii3;
CREATE DATABASE ejemployii3;
USE ejemployii3;
CREATE TABLE flores(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(255) NOT NULL,
  descripcion blob,
  imagen varchar(80)
  );
INSERT INTO flores(nombre, descripcion, imagen) VALUES 
  ('flor1','Flor de campo','1'),
  ('flor2','Flor de bosque','2'),
  ('flor3','Rosa','3'),
  ('flor4','Margarita','4'),
  ('flor5','Amapola','5'),
  ('flor6','Orquidea','6'),
  ('flor7','Clavel','7'),
  ('flor8','Cirio','8'),
  ('flor9','Elecho','9'),
  ('flor10','Prao','10'),
  ('flor11','Cardia','11'),
  ('flor12','R�cula','12'),
  ('flor13','T�','13')
  ;

SELECT * FROM flores;


SELECT COUNT(*) FROM FLORES;

SELECT * FROM flores ORDER BY RAND() LIMIT 1;
