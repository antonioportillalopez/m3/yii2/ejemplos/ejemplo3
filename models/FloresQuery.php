<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Flores]].
 *
 * @see Flores
 */
class FloresQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Flores[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Flores|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
