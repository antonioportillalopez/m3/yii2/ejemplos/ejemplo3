<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flores".
 *
 * @property int $id
 * @property string $nombre
 * @property resource|null $descripcion
 * @property string|null $imagen
 */
class Flores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'flores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 255],
            [['imagen'], 'string', 'max' => 80],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'imagen' => 'Imagen',
        ];
    }

    /**
     * {@inheritdoc}
     * @return FloresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FloresQuery(get_called_class());
    }
}
