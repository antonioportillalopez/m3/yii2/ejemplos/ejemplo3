<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider; //se añade para poder utilizar la clase dataprovider
use app\models\Flores; // Se añade para utilizar la clase del modelo flores


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /*
     * Se crea esta accion para el menu de la aplicación
     * Catalogo
     * 
     * 
     * 
     * 
     *  para que funcione hay que utilizar el USE de la clase utilidad Activedatarpvide y la clase del modelo de la tabla de la base de datos flores
     */
    public function actionCatalogo() 
    {
        // $a=Flores::find()->all(); //array de modelos (activeRecord)
        $b=Flores::find()->one(); //un modelo (ActiveRecord)
        $c= Flores::find(); // ActiveQuery
        
        $tabla = new ActiveDataProvider([
            'query' => Flores::find(),//creaamos el activedataprovider con el contenido completo de la tabla
        ]);//active Query con: one()-> es active Record
        
        return $this->render('catalogo', [
            'datos' => $tabla,
            'pagination' => [
                'pageSize' => 5,
                ]
        ]);
        
    }

    /*
     * Se crea esta accion para el menu de la aplicación
     * Recomendar
     */
    public function actionRecomendar() 
    {
    
        
    $n_elementos= Flores::find()->count();
        
        $posicion_registro_aleatoria= random_int(0, $n_elementos-1 );
    
    
    // $idealeatorio= rand(1,13);
    
        
        
    /*$consulta=Flores::find()
            ->offset(4)
            ->limit(1);*/
          
        
        $consulta= Flores::find();
    
    var_dump($consulta);
    $aleatorio = new ActiveDataProvider([
            'query' => $consulta,//creaamos el activedataprovider con el contenido completo de la tabla
            'pagination'=>[
                'pageSize'=>1,
                'page'=>$posicion_registro_aleatoria,
            ]
        ]);
    
    
        
        return $this->render('recomendar', [
            'datos' => $aleatorio,
        ]); 
    }
    
}
