<?php

use yii\helpers\Html;
use yii\grid\GridView;
    
// var_dump($tabla); // VEMOS EL OBJETO QUE HA GENERADO ACTIVE DATA PROVIDER, EN ESTE CASO LA TABLA COMPLETA



echo GridView::widget([ // no es necesario instanciar la clase para utilizar sus metodos
        'dataProvider' => $datos, //enviamos el activedataprovider 
        'columns' => [ // ponemos las columnas que queremos mostrar
           // ['class' => 'yii\grid\SerialColumn'], //columnas para saber el numero de registros

            'id', // estas columnas pertenecen al objeto dataProvider que vienen del modelo Flores que en este caso es el contenido entero de la tabla
            // ............los campos  no son necesarios los pone por defecto  ...........
            [
                'attribute'=>'nombre',
                'label'=>'Nombre completo del producto',
                'content'=>function($modelo){ //'contentOptions' y 'value' otra forma 
                    return $modelo->nombre;
                    }
                ],
            'descripcion',
            'imagen',// ejemplo el campo de otra tabla relacionada almacen.nombre

           // ['class' => 'yii\grid\ActionColumn'],// columnas especiales para añadir botones
            
        ],
    ]); 

?>